this_file = "venv/bin/activate_this.py"
exec(open(this_file).read(), {'__file__': this_file})

from flask import Flask,render_template,request,session
import uuid
import random
import json

class Question:
    uuid: str
    question: str
    reponse: list
    total_proposition: list
    proposition: list
    nombre_proposition: int

    def __init__(self, line=None):
        if line == None:
            pass
        else :
            global namespace
            sline = line.split("&")
            print(sline)
            self.uuid = uuid.uuid5(namespace, sline[0])
            self.question = sline[0]
            self.reponse = [x.strip() for x in sline[1].split(";")]
            self.nombre_proposition = sline[2]
            self.total_proposition = [x.strip() for x in sline[3].split(";")]
            self.proposition = None

    def set_propositions(self):
        self.proposition = random.sample(self.total_proposition, int(self.nombre_proposition)-len(self.reponse))
        for rep in self.reponse:
            self.proposition.insert(random.randrange(len(self.proposition)+1), rep)

def parse_question():
    questionlist = []
    with open("question") as question:
        for line in question:
            questionlist.append(Question(line))
    return questionlist

def check_result(resp):
    res = {}
    score = 0
    for uuid in resp.getlist('uuid'):
        err = False
        res[uuid] = resp.getlist(uuid)
        print(uuid, questions_dict)
        ques = questions_dict[uuid]["reponse"]
        for ans in resp.getlist(uuid):
            if ans not in ques:
                err = True
        for ans in ques:
            if ans not in resp.getlist(uuid):
                err = True
        if err:
            score -= 1
        else:
            score += 1
    return res, score

global namespace
namespace = uuid.uuid4()
questions = parse_question()
questions_dict = {}
nbr_question = 5
for question in questions:
    questions_dict[str(question.uuid)] = question.__dict__
    questions_dict[str(question.uuid)]["uuid"] = str(questions_dict[str(question.uuid)]["uuid"])


application = Flask(__name__)
application.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@application.route("/", methods=['GET', 'POST'])
def hello_world():
    if request.method == 'POST':
        res, score = check_result(request.form)
        test = json.loads(session["questions"])
        return render_template('result.html', questions=test, result=res, score=[nbr_question, score])
    else:
        questionss = random.sample(questions, nbr_question)
        for question in questionss:
            question.set_propositions()
        questions_dict = {}
        for question in questionss:
            questions_dict[str(question.uuid)] = question.__dict__
            questions_dict[str(question.uuid)]["uuid"] = str(questions_dict[str(question.uuid)]["uuid"])
        session["questions"] = json.dumps(questions_dict)
        return render_template('qcm.html', questions=questions_dict)
